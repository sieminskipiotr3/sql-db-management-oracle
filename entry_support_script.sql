-- Script supports user to enter information into Emp table

CONNECT your_username/your_password AS SYSDBA;

ACCEPT EMPNO NUMBER PROMPT 'Enter employee id: ';

ACCEPT ENAME CHAR PROMPT 'Enter employee name: ';

ACCEPT JOB CHAR PROMPT 'Enter position: ';

ACCEPT MGR NUMBER PROMPT 'Enter manager id: ';

ACCEPT HIREDATE DATE FORMAT 'DD-MON-YYYY' PROMPT 'Enter hire date format DD-MON-YYYY:  ';

ACCEPT SAL NUMBER PROMPT 'Enter monthly salary: ';
 
ACCEPT COMM NUMBER PROMPT 'Enter commission: ';

ACCEPT DEPTNO NUMBER PROMPT 'Enter department number: ';

BEGIN

INSERT INTO EMP VALUES
(%EMPNO, %ENAME, %JOB, %MGR,
TO_DATE(%HIREDATE, 'DD-MON-YYYY'), %SAL, %COMM, %DEPTNO);

COMMIT;

END;
/
PRINT EMPNO;
PRINT ENAME;
PRINT JOB;
PRINT MGR;
PRINT HIREDATE;
PRINT SAL;
PRINT COMM;
PRINT DEPTNO;
/
DISCONNECT;
