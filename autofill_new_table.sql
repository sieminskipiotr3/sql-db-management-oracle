-- This schema enters data into new table which keeps record of unique department numbers and its employees
-- Example: Deptno: 10 Employees: CLARK$KING$MILLER$

CREATE TABLE Emps_in_Depts (
	Deptno NUMBER(2),
	Employees VARCHAR2(4000)
);

SET SERVEROUTPUT ON;

DECLARE

CURSOR employees_data
IS SELECT * FROM Emp;

e_records employees_data%ROWTYPE;

CURSOR departments
IS SELECT DISTINCT Deptno INTO unique_departments FROM Dept;

unique_departments departments%ROWTYPE;

names VARCHAR2(4000);

BEGIN
OPEN employees_data;
OPEN departments;

FETCH employees_data INTO e_records;
EXIT WHEN employees_data%NOTFOUND;

FETCH departments INTO unique_departments;
EXIT WHEN departments%NOTFOUND;

FOR department IN unique_departments LOOP
	FOR employee IN e_records LOOP 
		IF e_records.Deptno == department.Deptno THEN
			names := CONCAT(CONCAT(&names, e_records.ENAME), "$");
		ELSE 
			CONTINUE;
	END LOOP;
	IF names IS NULL THEN
		INSERT INTO Emps_in_Depts VALUES(department, "");
	ELSE
		INSERT INTO Emps_in_Depts VALUES(department, names);
	names := NULL;
	COMMIT;
END LOOP;

EXCEPTION
	WHEN invalid_cursor THEN 
		DBMS_OUTPUT.Put_line("Cursos logic fixing.")
	WHEN cursor_already_open THEN
		 DBMS_OUTPUT.Put_line("Cursos already opened.")
	WHEN OTHERS THEN 
	  message := 'Error nr.= ' || SQLCODE|| ',message= ' || Substr(SQLERRM,1,100);
	  INSERT INTO Log VALUES (message);
	  DBMS_OUTPUT.Put_line('Other error appeared');


CLOSE employees_data;
CLOSE departments;

END;
/
